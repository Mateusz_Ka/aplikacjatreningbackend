package trening;

import org.springframework.data.repository.CrudRepository;
public interface ExcercisesRepository extends CrudRepository<Excercises, Integer>  {

    Iterable<Excercises> findByPlanId(Integer planId);
}
