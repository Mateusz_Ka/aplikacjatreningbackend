package trening;
import javax.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "trening")
public class Trening {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    private Integer planId;

    private boolean oneSide;

    @Column()
    private Long timestamp;

    @Column()
    private String name;
    @Column()
    private String series;
    @Column()
    private String times;
    @Column()
    private String leftSideTImes;
    @Column()
    private String rightSideTimes;
    @Column()
    private String weight;
    @Column()
    private String leftSideWidth;
    @Column()
    private String rightSideWidth;

    public String getRightSideWidth() {
        return rightSideWidth;
    }

    public void setRightSideWidth(String rightSideWidth) {
        this.rightSideWidth = rightSideWidth;
    }

    public String getLeftSideWidth() {
        return leftSideWidth;
    }

    public void setLeftSideWidth(String leftSideWidth) {
        this.leftSideWidth = leftSideWidth;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getRightSideTimes() {
        return rightSideTimes;
    }

    public void setRightSideTimes(String rightSideTimes) {
        this.rightSideTimes = rightSideTimes;
    }

    public String getLeftSideTImes() {
        return leftSideTImes;
    }

    public void setLeftSideTImes(String leftSideTImes) {
        this.leftSideTImes = leftSideTImes;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOneSide() {
        return oneSide;
    }

    public void setOneSide(boolean oneSide) {
        this.oneSide = oneSide;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
