package trening;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path="/plan")
public class TreningPlanController {
    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private  ExcercisesRepository excercisesRepository;

    @Autowired
    private  TreningRepository treningRepository;


    @PostMapping(path="/add")
    public @ResponseBody String addNewPlan (
            @RequestBody Plan plan
    ) {

        System.out.print(plan);
        planRepository.save(plan);
        System.out.print("saved");
        return "DONE";
    }


    @GetMapping(path="/all")
    public @ResponseBody Iterable<Plan> getAllPlans() {
        return planRepository.findAll();
    }

    @PostMapping(path="/{id}/add")
    public @ResponseBody String addNewExcercise (
            @RequestBody Excercises excercises,
            @PathVariable Integer id
    ) {
        excercises.setPlanId(id);
        excercisesRepository.save(excercises);
        return "DONE";
    }

    @GetMapping(path="/{id}/all")
    public @ResponseBody Iterable<Excercises> getAllExcercises(
            @PathVariable Integer id) {
        return excercisesRepository.findByPlanId(id);
    }

    @PostMapping(path="/{id}/trening/done")
    public @ResponseBody String addNewTrening (
            @RequestBody Trening trening,
            @PathVariable Integer id
    ) {
        trening.setPlanId(id);
        treningRepository.save(trening);
        return "DONE";
    }

    @GetMapping(path="/{id}/trening/all")
    public @ResponseBody Iterable<Trening> getAllTrenings(
            @PathVariable Integer id) {
        return treningRepository.findByPlanId(id);
    }
}
