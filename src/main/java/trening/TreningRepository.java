package trening;

import org.springframework.data.repository.CrudRepository;
public interface TreningRepository extends CrudRepository<Trening, Integer>  {

    Iterable<Trening> findByPlanId(Integer planId);
}